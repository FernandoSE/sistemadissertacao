#ifndef _MLP_CONFIG_H_
#define _MLP_CONFIG_H_

#ifdef XOR_DATASET
#define DATASET_DIR "../XOR/"
#define DATASET_NAMES DATASET_DIR "xor.names"
#define DATASET_DATA DATASET_DIR "xor.data"
#define NUM_PATTERNS 4
#define NUM_FEATURES 3
#define NUM_CLASSES 2

typedef float featureInputType[NUM_FEATURES];
typedef float featureOutputType;
#endif

#ifdef KDD99
#define DATASET_DIR "../KDD99/"
#define DATASET_NAMES DATASET_DIR "kddcup.names"
// #define DATASET_DATA DATASET_DIR "kddcup_100_results.data"
#define DATASET_DATA DATASET_DIR "kddcup_dos.data"
// #define NUM_PATTERNS 100
#define NUM_PATTERNS 488736
#define NUM_FEATURES 41
#define NUM_CLASSES 2

typedef float featureInputType[NUM_FEATURES];
typedef float featureOutputType;
#endif

#ifdef AWID
#define DATASET_DIR "../AWID/"
#define DATASET_NAMES DATASET_DIR "awid.names"
// #define DATASET_DATA DATASET_DIR "awid_100.data"
#define DATASET_DATA DATASET_DIR "awid.data"
// #define NUM_PATTERNS 100
#define NUM_PATTERNS 1795575
// #define NUM_PATTERNS 600000
#define NUM_FEATURES 154
#define NUM_CLASSES 2
// #define NUM_CLASSES 4

typedef float featureInputType[NUM_FEATURES];
typedef float featureOutputType;
#endif

#define SIZE_CLASS_STRING 20

#define NUM_INPUTS NUM_FEATURES
// #define NUM_HIDDEN 10
#define NUM_HIDDEN 25

/** Train values **/
// const int numEpochs = 100000;
// const int numEpochs = 50;
// const int numEpochs = 10;
//const int numEpochs = 10;
#define numEpochs (10)
// const double LR_IH = 0.7;
// const double LR_HO = 0.07;
// const double LR_IH = 0.1;
// const double LR_HO = 0.01;
// const double LR_IH = 0.01;
// const double LR_HO = 0.001;
//const double LR_IH = 0.001;
#define LR_IH (0.001)
//const double LR_HO = 0.0001;
#define LR_HO (0.0001)
// const double momentum = 0.001;
//const double momentum = 0.0;
#define momentum (0.0)
/******************/


#endif
