#include "../UniqueSet.h"
#include "CppUTest/TestHarness.h"
#include "CppUTest/CommandLineTestRunner.h"

UniqueSet* us;

TEST_GROUP(UniqueSet) {
  void setup() {
    us = UniqueSet_cria();
  }
  void teardown() {
    UniqueSet_libera(us);
  }
};

TEST(UniqueSet, TestInitSize) {
  CHECK_EQUAL(0, UniqueSet_tamanho(us));
}

TEST(UniqueSet, TestWithOneAdd) {
  int index;
  char c_test = 'a';
  CHECK_EQUAL(0, UniqueSet_tamanho(us));
  UniqueSet_adiciona(us, &c_test);
  CHECK_EQUAL(1, UniqueSet_tamanho(us));
  CHECK_EQUAL(0, index = UniqueSet_busca(us, &c_test));
  STRCMP_EQUAL(&c_test, UniqueSet_getValor(us, index));
}

TEST(UniqueSet, TestWithTwoClasses) {
  int index;
  char class1[10] = "normal";
  char class2[10] = "dos";

  CHECK_EQUAL(0, UniqueSet_tamanho(us));
  UniqueSet_adiciona(us, class1);
  CHECK_EQUAL(1, UniqueSet_tamanho(us));
  UniqueSet_adiciona(us, class2);
  CHECK_EQUAL(2, UniqueSet_tamanho(us));

  CHECK_EQUAL(0, index = UniqueSet_busca(us, class1));
  STRCMP_EQUAL(class1, UniqueSet_getValor(us, index));

  CHECK_EQUAL(1, index = UniqueSet_busca(us, class2));
  STRCMP_EQUAL(class2, UniqueSet_getValor(us, index));
}

int main(int ac, char** av) {
  return CommandLineTestRunner::RunAllTests(ac,av);
}
