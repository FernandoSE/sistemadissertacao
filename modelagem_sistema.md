# Componentes

- MLP
- ACD

O MLP recebe como entrada um vetor de sinais, que serão estimulados e gerará, como saída, as entradas das células dendríticas.


node 0 MLP -> CD

node 1 MLP -> CD
 ...
node n MLP -> CD

As células dendríticas geradas irão migrar para o Linfonodo, que fará a análise das células dendríticas.


O treinamento da MLP, a princípio, será supervisionado. Utilizarem o KDD99 pelos resultados obtidos. É necessário ajustar a saída da base KDD99 para as entradas da CD (Talvez pegar alguma informação de uma das entradas como um sinal de saída).
Para iniciar a implementação é possível considerar que:

- Normal -> Sinal de Segurança
- DoS -> Sinal de Perigo
