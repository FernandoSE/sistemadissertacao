#include <vector>
#include <map>

#include "Util.hpp"

#ifndef PATTERN_HPP
#define PATTERN_HPP

class Pattern {
private:
public:
  char class_label;
  int input;
  double safe;
  double danger;

  Pattern(char class_label, std::map<char, std::vector<int> > domain, double p_safe, double p_danger);
  ~Pattern();
};

Pattern::Pattern(char class_label, std::map<char, std::vector<int> > domain, double p_safe, double p_danger) {
  std::vector<int> set = domain[class_label];
  int selection = (int)(getRand()*set.size());

  this->class_label = class_label;
  this->input = set[selection];
  this->safe = (getRand() * p_safe * 100);
  this->danger = (getRand() * p_danger * 100);
}

Pattern::~Pattern() {
}

Pattern* generate_pattern(std::map<char, std::vector<int> > domain, double p_anomaly, double p_normal, double prob_create_anom=0.5) {
  Pattern* p;

  if (getRand() < prob_create_anom) {
    p = new Pattern('A', domain, 1.0-p_normal, p_anomaly);
  } else {
    p = new Pattern('N', domain, p_normal, 1.0-p_anomaly);
  }

  return p;
}

#endif
