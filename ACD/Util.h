#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#ifndef UTIL_H_
#define UTIL_H_

#define NUM_ANTIGENS 50

double getRand(void) {
  return ((double) rand())/(double)RAND_MAX;
}

int rand_in_bounds_int(int min, int max) {
  return (rand()%(max-min)) + min;
}

double rand_in_bounds(double min, double max) {
  return (min + (max-min)*getRand());
}

double* random_vector(double** search_space, int n) {
  double* v = (double*)malloc(n*sizeof(double));
  int i;

  for(i=0; i<n; i++){
    v[i] = rand_in_bounds(search_space[i][0], search_space[i][1]);
  }

  return v;
}

void create_domain_map(int* domain) {
  int i;
  domain = (int*) malloc(NUM_ANTIGENS*sizeof(int));

  for(i=0; i<NUM_ANTIGENS; i++) {
    domain[i] = i;
  }
}

#endif
