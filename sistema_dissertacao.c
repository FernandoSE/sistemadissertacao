#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#include "./MLP/MLP_config.h"
#include "./MLP/MLP.h"

#include "./ACD/DCA.h"

#ifdef XOR_DATASET
#include "./XOR/xor.h"
#endif

#ifdef KDD99
#include "./KDD99/kdd99.h"
#endif

#ifdef AWID
#include "./AWID/awid.h"
#endif


#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"

int resultados[NUM_CLASSES][NUM_CLASSES];
int resultadoslw[NUM_CLASSES][NUM_CLASSES];

int main() {
    MLP mlp;
    double thresh[2] = {5, 15};
    unsigned int num_cells = 10;
    Cell** immature_cells = (Cell**) malloc(num_cells*sizeof(Cell*));
    Lymph* l = Lymph_init(immature_cells, thresh);

    srand( time(NULL) );

    initTrainData();

    MLP_initWeights(&mlp);

    int i;
    int j;

    long long int certosMLP = 0;
    long long int falsosPositivosMLP = 0;
    long long int totalMLP = 0;

    long long int certos = 0;
    long long int falsosPositivos = 0;
    long long int total = 0;
    for(j=0; j<10; j++) {
        for(i=0; i<NUM_PATTERNS; i++) {
            featureInputType trainInput;
            featureOutputType trainOutput;

            getTrainInput(&trainInput, i);
            getTrainOutput(&trainOutput, i);

            MLP_train(&mlp, trainInput, trainOutput);
            
            double output = MLP_predict(&mlp, trainInput);

            int expected = (int)round(((trainOutput+1.0)/(2.0))*(NUM_CLASSES-1.0));
            int calculated = (int)round(((output+1.0)/(2.0))*(NUM_CLASSES-1.0));

            if(expected == calculated){
                certosMLP += 1;
            }
            
            if(expected == 1 && calculated != 1){
                falsosPositivosMLP += 1;
            }
            
            totalMLP += 1;

            Pattern* p = new_Pattern((output>0)?('N'):('A'), i, (output>0)?(output):(0), (output < 0)?(-output):(0));

            Lymph_expose_all_cells(l, p, thresh);

            if(l->migrate_size >= 3) {
                double mcav = Lymph_check_attack(l);
                Lymph_clean_cells(l);

                //printf("Lymph -> %f(A>0.5) Saída: %f(A<0)\n", mcav, output);
                if((mcav >= 0.5 && trainOutput <= 0) || (mcav <= 0.5 && trainOutput >= 0)) {
                    certos++;
                } else if ((mcav >= 0.5 && trainOutput > 0)) {
                    falsosPositivos++;
                }
                
                total++;
            }

        }
    }

    printf("CertosMLP/FalsosPositivosMLP/TotalMLP: %lld/%lld/%lld\n", certosMLP, falsosPositivosMLP, totalMLP);
    printf("Certos/FalsosPositivos/Total: %lld/%lld/%lld\n", certos, falsosPositivos, total);

    printf("MLP: %f%% (Falsos Positivos: %f%%)\n", ((double)certosMLP/(double)totalMLP)*100.0, ((double)falsosPositivosMLP/(double)totalMLP)*100.0);
    printf("Sistema: %f%% (Falsos Positivos: %f%%)\n", ((double)certos/(double)total)*100.0, ((double)falsosPositivos/(double)total)*100.0);

    /*
        int e, i;

        for(e=0; e<=numEpochs; e++){
        printf("epoca %d...", e);
            double RMSerror = 0.0;

            // batalada
            for(i=0; i<NUM_PATTERNS; i++){
        featureInputType trainInput;
        featureOutputType trainOutput;

                getTrainInput(&trainInput, i);
                getTrainOutput(&trainOutput, i);
                double error = MLP_train(&mlp, trainInput, trainOutput);

                RMSerror = RMSerror + error*error;
            }

            // Calcula Erro
            RMSerror = sqrt(RMSerror/(double)NUM_PATTERNS);

            // Imprime o erro
        printf("\t\t... erro RMS %f\n", RMSerror);

        // Verifica se sai do treinamento
            if(RMSerror < 0.0001){
                break;
            }
        }

    for(i=0; i<NUM_CLASSES; i++){
        int j;
        for(j=0; j<NUM_CLASSES; j++){
        resultados[i][j] = 0;
        resultadoslw[i][j] = 0;
        }
    }

    int certos = 0;
    int total = 0;

    int falsosPositivos = 0;

    FILE* logNeuralModel;

    logNeuralModel = fopen("neuralOutput.log", "w");

        for(i=0; i<NUM_PATTERNS; i++){
        featureInputType trainInput;
        featureOutputType trainOutput;
            getTrainInput(&trainInput, i);
            getTrainOutput(&trainOutput, i);

            double output = MLP_predict(&mlp, trainInput);
        fprintf(logNeuralModel, "pat = %d actual = %f neural model = %f\n", i, trainOutput, output);

        int expected = (int)round(((trainOutput+1.0)/(2.0))*(NUM_CLASSES-1.0));
        int calculated = (int)round(((output+1.0)/(2.0))*(NUM_CLASSES-1.0));

            if(expected == calculated){
                certos += 1;
            }
            if(expected == 1 && calculated != 1){
                falsosPositivos += 1;
            }
            total += 1;
        resultados[expected][calculated] += 1;
        }

    fclose(logNeuralModel);

        MLP_print(&mlp);

        printf("Esperado\\Resultado\n");
        printf("  |");
        for(i=0; i<NUM_CLASSES; i++){
            printf("%2d|", i);
        }
        printf("\n");
        for(i=0; i<NUM_CLASSES; i++){
            int j;
            printf("%2d|", i);
            for(j=0; j<NUM_CLASSES; j++){
                if(i==j){
                    printf(KGRN);
                } else if(resultados[i][j] != 0){
                    printf(KMAG);
                }
                printf("%2d|", resultados[i][j]);
                if(i==j){
                    printf(RESET);
                } else if(resultados[i][j] != 0){
                    printf(RESET);
                }
            }
            printf("\n");
        }
        printf("\n");
    */

    return 0;
}
