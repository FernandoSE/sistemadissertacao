/*
 *	Class UniqueSet
 */

#ifndef DATA_STR_LEN
#define DATA_STR_LEN (40)
#endif

typedef struct {
	int tamanho;
	char **data;
} UniqueSet;

UniqueSet* UniqueSet_cria(void);
void UniqueSet_libera(UniqueSet* uset);
int UniqueSet_tamanho(UniqueSet *uset);
int UniqueSet_adiciona(UniqueSet *uset, const char *value);
char* UniqueSet_getValor(UniqueSet *uset, const int id);
int UniqueSet_busca(UniqueSet* uset, const char* value);
