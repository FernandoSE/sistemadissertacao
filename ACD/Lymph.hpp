#include <vector>

#ifndef LYMPH_HPP
#define LYMPH_HPP

class Lymph {
private:
public:
  std::vector<Cell*> *cells;
  std::vector<Cell*> *migrate;

  Lymph(std::vector<Cell*> &cells);

  void expose_all_cells(Pattern *p, double threshold[2]);

  char classify_pattern(Pattern *p);
};

Lymph::Lymph(std::vector<Cell*> &cells) {
  this->cells = &cells;
  this->migrate = new std::vector<Cell*>();
}

void Lymph::expose_all_cells(Pattern *p, double threshold[2]) {

  double cms = p->safe + p->danger;
  double k = p->danger - (p->safe * 2.0);

  for(unsigned int i=0; i<this->cells->size(); i++) {
    Cell *c = this->cells->at(i);
    c->expose_cell(cms, k, *p, threshold);

    if(c->can_cell_migrate()) {
      c->class_label = (c->k > 0)?'A':'N';
      this->migrate->push_back(new Cell(c));

      // this->cells->erase(this->cells->begin() + i);
      // this->cells->insert(this->cells->begin() + i, new Cell(threshold));
      this->cells->at(i) = new Cell(threshold);
    }
  }

}

char Lymph::classify_pattern(Pattern *p) {
  int input = p->input;
  unsigned int num_cells = 0;
  unsigned int num_antigen = 0;

  for(auto it = this->migrate->begin(); it != this->migrate->end(); it++) {
    Cell* c = *it;

    if(c->class_label == 'A' && c->antigen[input]) {
      num_cells += 1;
      num_antigen += c->antigen[input];
    }
  }

  double mcav = (double)num_cells / (double) num_antigen;

  return (mcav>0.5)?'A':'N';
}

#endif
