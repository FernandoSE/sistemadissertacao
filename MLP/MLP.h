#ifndef _MLP_H_
#define _MLP_H_

#include "MLP_config.h"

typedef struct _MLP{
	double weightsIH[NUM_INPUTS][NUM_HIDDEN];
	double weightsHO[NUM_HIDDEN];
} MLP;

/******************/

void MLP_initWeights(MLP* mlp);
double MLP_train(MLP* mlp, featureInputType input, featureOutputType outputDesire);
double MLP_predict(MLP* mlp, featureInputType input);
void MLP_print(MLP* mlp);

double sigmoid(double x);

#endif
