#include "Util.h"
#ifndef PATTERN_H_
#define PATTERN_H_

typedef struct {
  char class_label;
  int input;
  double safe;
  double danger;
} Pattern;

Pattern* new_Pattern(char class_label, int input, double safe, double danger) {
  Pattern* this = (Pattern*) malloc(sizeof(Pattern));

  this->class_label = class_label;
  this->input = input;
  this->safe = safe;
  this->danger = danger;

  return this;
}

Pattern* Pattern_init(char class_label, int* domain, double p_safe, double p_danger) {
  Pattern* this = (Pattern*) malloc(sizeof(Pattern));

  /*
  std::vector<int> set = domain[class_label];
  */
  int selection;

  if(class_label == 'N') {
    selection = rand_in_bounds_int(0, 45);
  } else {
    selection = rand_in_bounds_int(45, 50);
  }


  this->class_label = class_label;
  this->input = selection;
  this->safe = (getRand() * p_safe * 100);
  this->danger = (getRand() * p_danger * 100);

  return this;
}

Pattern* Pattern_generate_pattern(int* domain, double p_anomaly, double p_normal) {
  Pattern* p;

  double prob_create_anom = 0.5;

  if (getRand() < prob_create_anom) {
    p = Pattern_init('A', domain, 1.0-p_normal, p_anomaly);
  } else {
    p = Pattern_init('N', domain, p_normal, 1.0-p_anomaly);
  }

  return p;
}


#endif
