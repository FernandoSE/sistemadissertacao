#ifndef _XOR_H_
#define _XOR_H_

void initTrainData();
void getTrainInput(featureInputType* input, int index);
void getTrainOutput(featureOutputType* output, int index);

#endif
