#include <stdio.h>
#include <stdlib.h>

#include "Util.h"
#include "Pattern.h"

#ifndef CELL_H_
#define CELL_H_

typedef struct {
  char class_label;
  double lifespan;
  double k;
  double cms;
  double migration_threshold;
  int antigen[NUM_ANTIGENS];
} Cell;

Cell* Cell_init(double thresh[2]) {
  Cell* this = (Cell*)malloc(sizeof(Cell));

  this->lifespan = 1000.0;
  this->k = 0.0;
  this->cms = 0.0;
  this->migration_threshold = rand_in_bounds(thresh[0], thresh[1]);
  //this->antigen;

  int i;
  for(i=0; i<NUM_ANTIGENS; i++) {
    this->antigen[i] = 0;
  }

  return this;
}

void Cell_store_antigen(Cell* this, int input) {
  if(!this->antigen[input]) {
    this->antigen[input] = 1;
  } else {
    this->antigen[input] += 1;
  }
}

void Cell_expose_cell(Cell* this, double cms, double k, const Pattern pattern, double threshold[2]) {
  this->cms += cms;
  this->k += k;
  this->lifespan -= cms;
  //Cell_store_antigen(this, pattern.input);

  if(this->lifespan <= 0.0) {
    this->lifespan = 1000.0;
    this->k = 0.0;
    this->cms = 0.0;
    this->migration_threshold = rand_in_bounds(threshold[0], threshold[1]);
    int i;
    for(i=0; i<NUM_ANTIGENS; i++) {
      this->antigen[i] = 0;
    }
  }
}

char Cell_can_cell_migrate(Cell* this) {
  char antigen_empty = 0;

  int i;
  for(i=0; i<NUM_ANTIGENS; i++) {
    if(this->antigen[i] != 0) {
      antigen_empty = 0;
      break;
    }
  }

  return this->cms >= this->migration_threshold && !antigen_empty;
}

#endif
