#include <stdio.h>
#include <vector>

#include "Util.hpp"
#include "Pattern.hpp"

#ifndef CELL_HPP
#define CELL_HPP

class Cell {
private:
public:
  char class_label;
  double lifespan;
  double k;
  double cms;
  double migration_threshold;
  std::vector<int> antigen;

  Cell(double thresh[2]);
  Cell(Cell* c);
  ~Cell();

  void store_antigen(int input);
  void expose_cell(double cms, double k, const Pattern pattern, double threshold[2]);
  bool can_cell_migrate();

};

Cell::Cell(double thresh[2]) {
  this->lifespan = 1000.0;
  this->k = 0.0;
  this->cms = 0.0;
  this->migration_threshold = rand_in_bounds(thresh[0], thresh[1]);
  this->antigen = std::vector<int>(NUM_ANTIGENS);
}

Cell::Cell(Cell* c) {
  this->class_label = c->class_label;
  this->lifespan = c->lifespan;
  this->k = c->k;
  this->cms = c->cms;
  this->migration_threshold = c->migration_threshold;
  this->antigen = std::vector<int>(NUM_ANTIGENS);

  for(unsigned int i=0; i<NUM_ANTIGENS; i++) {
    this->antigen[i] = c->antigen[i];
  }
}

Cell::~Cell() {
}

void Cell::store_antigen(int input) {
  if(!this->antigen[input]) {
    this->antigen[input] = 1;
  } else {
    this->antigen[input] += 1;
  }
}

void Cell::expose_cell(double cms, double k, const Pattern pattern, double threshold[2]) {
  this->cms += cms;
  this->k += k;
  this->lifespan -= cms;
  this->store_antigen(pattern.input);

  if(this->lifespan <= 0.0) {
    this->lifespan = 1000.0;
    this->k = 0.0;
    this->cms = 0.0;
    this->migration_threshold = rand_in_bounds(threshold[0], threshold[1]);
    this->antigen = std::vector<int>(NUM_ANTIGENS);
  }
}

bool Cell::can_cell_migrate() {
  return this->cms >= this->migration_threshold && !this->antigen.empty();
}

#endif
