#include <map>
#include <vector>

#ifndef UTIL_HPP
#define UTIL_HPP

#define NUM_ANTIGENS 50

double getRand(void) {
  return ((double) rand())/(double)RAND_MAX;
}

double rand_in_bounds(double min, double max) {
  return (min + (max-min)*getRand());
}

double* random_vector(double** search_space, int n) {
  double* v = (double*)malloc(n*sizeof(double));
  int i;

  for(i=0; i<n; i++){
    v[i] = rand_in_bounds(search_space[i][0], search_space[i][1]);
  }

  return v;
}

void create_domain_map(std::map< char, std::vector<int> > &domain) {
  std::vector<int> normal;
  std::vector<int> anomaly;

  for(int i=0; i<50; i++) {
    if (i % 10 == 0) {
      anomaly.push_back(i);
    } else {
      normal.push_back(i);
    }
  }

  domain['N'] = normal;
  domain['A'] = anomaly;

}

#endif
