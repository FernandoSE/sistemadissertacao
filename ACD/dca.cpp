#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>

#include <string>
#include <vector>
#include <map>

#include "Util.hpp"
#include "Cell.hpp"
#include "Pattern.hpp"
#include "Lymph.hpp"

int main() {
  std::map<char, std::vector<int> > domain;

  create_domain_map(domain);

  double p_anomaly = 0.70;
  double p_normal = 0.95;

  //unsigned int iterations = 100;
  unsigned int iterations = 100;
  unsigned int num_cells = 10;

  double thresh[2] = {5, 15};

  for(auto it_map: domain) {
    printf("%c:\n", it_map.first);

    for(auto it: it_map.second) {
      printf("%d,", it);
    }
    printf("\n");
  }

  /* train_system */
  std::vector<Cell*> immature_cells;

  for(unsigned int i=0; i<num_cells; i++) {
    immature_cells.push_back(new Cell(thresh));
  }

  Lymph* lymph = new Lymph(immature_cells);

  for(unsigned int i=0; i<iterations; i++) {
    Pattern* p = generate_pattern(domain, p_anomaly, p_normal);

    lymph->expose_all_cells(p, thresh);

  }
  printf("Migrated size: %lu\n", lymph->migrate->size());
  printf("Cells size: %lu\n", lymph->cells->size());
  /* train_system */

  /* test_system */
  unsigned int num_trial = 100;
  unsigned int correct_norm = 0;

  for(unsigned int i=0; i<num_trial; i++) {
    Pattern *p = new Pattern('N', domain, p_normal, 1.0 - p_anomaly);

    char class_label = lymph->classify_pattern(p);

    correct_norm += (class_label == 'N')?1:0;
  }
  printf("Finished testing Normal inputs %d/%d\n", correct_norm, num_trial);

  unsigned int correct_anom = 0;

  for(unsigned int i=0; i<num_trial; i++) {
    Pattern *p = new Pattern('A', domain, 1.0 - p_normal, p_anomaly);

    char class_label = lymph->classify_pattern(p);

    correct_anom += (class_label == 'A')?1:0;
  }
  printf("Finished testing Anomaly inputs %d/%d\n", correct_anom, num_trial);

  /* test_system */

  return 0;
}
