#include "../MLP/MLP_config.h"

static featureInputType trainInput[NUM_PATTERNS] =
#include "input.data"
;

static featureOutputType trainOutput[NUM_PATTERNS] =
#include "output.data"
;

void initTrainData() {
  /* Nothing to do */
}

void getTrainInput(featureInputType* input, int index) {
  int i;
  for(i=0; i<NUM_INPUTS; i++) {
    (*input)[i] = trainInput[index][i];
  }
}

void getTrainOutput(featureOutputType* output, int index) {
  (*output) = trainOutput[index];
}
