#include <stdlib.h>
#include <string.h>
#include "UniqueSet.h"

UniqueSet* UniqueSet_cria(void){
	UniqueSet *uset = (UniqueSet*) malloc(sizeof(UniqueSet));
	uset->tamanho = 0;

	uset->data = (char**) malloc(1*sizeof(char*));
	uset->data[0] = (char*) malloc(DATA_STR_LEN*sizeof(char));


	return uset;
}

void UniqueSet_libera(UniqueSet* uset){
	int i;
	for(i=0; i<uset->tamanho; i++){
		free(uset->data[i]);
	}
	free(uset->data);

	free(uset);
}

int UniqueSet_tamanho(UniqueSet *uset){
	return uset->tamanho;
}

int UniqueSet_adiciona(UniqueSet *uset, const char *value){
	int id = UniqueSet_busca(uset, value);
	if(id == -1){
		id = uset->tamanho;

		uset->tamanho += 1;
		uset->data = (char**) realloc(uset->data, uset->tamanho*sizeof(char*));
		uset->data[id] = (char*) malloc(DATA_STR_LEN*sizeof(char));
		//strncpy(uset->data[id], value, strlen(value));
		// strcpy(uset->data[id], value);
		strncpy(uset->data[id], value, DATA_STR_LEN);
	}

	return id;
}

char* UniqueSet_getValor(UniqueSet *uset, const int id){
	return uset->data[id];
}

int UniqueSet_busca(UniqueSet* uset, const char* value){
	int id;

	for(id=uset->tamanho-1; id>=0; id--){
		if(strncmp(uset->data[id], value, strlen(value)) == 0){
			break;
		}
	}

	return id;
}
