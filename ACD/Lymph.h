#ifndef LYMPH_H_
#define LYMPH_H_

#define NUM_CELLS 10
#define MAX_NUM_CELLS 1000

typedef struct {
  Cell *cells[NUM_CELLS];
  Cell *migrate[MAX_NUM_CELLS];
  int migrate_size;
} Lymph;

Lymph* Lymph_init(Cell **cells, double threshold[2]) {
  Lymph* this = (Lymph*) malloc(sizeof(Lymph));

  //this->cells = cells;
  int i;
  for(i=0; i<NUM_CELLS; i++) {
    this->cells[i] = Cell_init(threshold);
  }
  this->migrate_size = 0;

  return this;
}

void Lymph_expose_all_cells(Lymph* this, Pattern *p, double threshold[2]) {

  double cms = p->safe + p->danger;
  double k = p->danger - (p->safe * 2.0);

  unsigned int i;
  for(i=0; i<NUM_CELLS; i++) {
    Cell *c = this->cells[i];
    Cell_expose_cell(c, cms, k, *p, threshold);

    if(Cell_can_cell_migrate(c)) {
      c->class_label = (c->k > 0)?'A':'N';
      //this->migrate->push_back(new Cell(c));
      this->migrate[this->migrate_size] = c;
      ++(this->migrate_size);

      this->cells[i] = Cell_init(threshold);
    }
  }

}

double Lymph_check_attack(Lymph* this) {
  unsigned int i;

  unsigned int normal_cells = 0;
  unsigned int annomaly_cells = 0;
  for(i=0; i<this->migrate_size; i++) {
    Cell *c = this->migrate[i];

    if(c->class_label == 'A') {
      annomaly_cells++;
    } else {
      normal_cells++;
    }
  }

  return (double)annomaly_cells / ((double)annomaly_cells+(double)normal_cells);
}

void Lymph_clean_cells(Lymph* this) {
  this->migrate_size = 0;
}

char Lymph_classify_pattern(Lymph* this, Pattern *p) {
  int input = p->input;
  unsigned int num_cells = 0;
  unsigned int num_antigen = 0;

  int i;
  for(i = 0; i<this->migrate_size; i++) {
    Cell* c = this->migrate[i];

    if(c->class_label == 'A' && c->antigen[input]) {
      num_cells += 1;
      num_antigen += c->antigen[input];
    }
  }

  double mcav = (double)num_cells / (double) num_antigen;

  return (mcav>0.5)?'A':'N';
}


#endif
