CC = gcc
CPPFLAGS = -Wall -g
#INCLUDES = -I../include
#LFLAGS = -L$(CPPUTEST_HOME)/lib
#LIBS = -lCppUTest
LIBS = -lm
# SRCS = testMLP.c ./MLP/MLP.c# ./UniqueSet/UniqueSet.c ./MLP/MLP.c
# OBJS = testMLP.o ./MLP/MLP.o# ./UniqueSet/UniqueSet.o ./MLP/MLP.o
# MAIN = testMLP

MAIN = sistema_dissertacao
SRCS = $(MAIN).c ./MLP/MLP.c
OBJS = $(MAIN).o ./MLP/MLP.o

DATASET = KDD99
# DATASET = XOR_DATASET
# DATASET = AWID

ifeq ($(DATASET),XOR_DATASET)
SRCS += ./XOR/xor.c
OBJS += ./XOR/xor.o
else
ifeq ($(DATASET),KDD99)
SRCS += ./KDD99/kdd99.c
OBJS += ./KDD99/kdd99.o
else
ifeq ($(DATASET),AWID)
SRCS += ./AWID/awid.c
OBJS += ./AWID/awid.o
else
endif
endif
endif

.PHONY: all clean

all: $(MAIN)
	@echo Training Sistema Dissertacao
	./$(MAIN)

compile: $(MAIN)

$(MAIN): $(OBJS)
	@echo $(CC) $(CPPFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS) -D$(DATASET)
	$(CC) $(CPPFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS) -D$(DATASET)
	@echo $(MAIN) Compilado com sucesso!

#.cpp.o:
#	$(CC) $(CPPFLAGS) $(INCLUDES) -c $< -o $@

.c.o:
	$(CC) $(CPPFLAGS) $(INCLUDES) -c $< -o $@ -D$(DATASET)

clean:
	$(RM) ./*.o
	$(RM) ./**/*.o *~ $(MAIN)

debug: $(OBJS)
	@echo Debugging Sistema Dissertacao
	@echo $(CC) -g $(CPPFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS) -D$(DATASET)
	$(CC) -g $(CPPFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS) -D$(DATASET)
	gdb ./$(MAIN)

#depend:
