#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "MLP.h"
#include "MLP_config.h"

/** Train values **/
// extern const int numEpochs;
// extern const double LR_IH;
// extern const double LR_HO;
// extern const double momentum;
/******************/

#define min(x,y) (((x)<(y))?(x):(y))
#define max(x,y) (((x)>(y))?(x):(y))

double getRandMLP(void){
	return ((double)rand())/(double)RAND_MAX;
}

void MLP_initWeights(MLP* mlp){
	int i, j;
	for(i=0; i<NUM_HIDDEN; i++){
		mlp->weightsHO[i] = (getRandMLP() - 0.5)/2;
		for(j=0; j<NUM_INPUTS; j++){
			mlp->weightsIH[j][i] = (getRandMLP() - 0.5)/5;
		}
	}
}

double MLP_train(MLP* mlp, featureInputType input, featureOutputType outputDesire) {
	int j, k;
	double hiddenVal[NUM_HIDDEN];

	double output = 0.0;

	double error;

	for(j=0; j<NUM_HIDDEN; j++) {
		hiddenVal[j] = 0.0;
		for(k=0; k<NUM_INPUTS; k++) {
			hiddenVal[j] += (input[k]*mlp->weightsIH[k][j]);
		}
		hiddenVal[j] = sigmoid(hiddenVal[j]);
	}

	for(j=0; j<NUM_HIDDEN; j++) {
		output += hiddenVal[j]*mlp->weightsHO[j];
	}

	error = output - outputDesire;

	for(j=0; j<NUM_HIDDEN; j++) {
		double weightChange = LR_HO * error * hiddenVal[j];

		mlp->weightsHO[j] -= weightChange;

		mlp->weightsHO[j] = max(-1.0, min(1.0, mlp->weightsHO[j]));
	}

	for(j=0; j<NUM_HIDDEN; j++) {
		for(k=0; k<NUM_INPUTS; k++) {
			double weightChange = 1 - (hiddenVal[j]*hiddenVal[j]);
			weightChange *= mlp->weightsHO[j] * error * LR_IH * input[k];
			mlp->weightsIH[k][j] -= weightChange;

			mlp->weightsIH[k][j] = max(-1.0, min(1.0, mlp->weightsIH[k][j]));
		}
	}

	return error;
}

double MLP_predict(MLP* mlp, featureInputType input) {
	double hiddenVal[NUM_HIDDEN];

	double output = 0.0;
	int j, k;

	for(j=0; j<NUM_HIDDEN; j++){
		hiddenVal[j] = 0.0;
		for(k=0; k<NUM_INPUTS; k++){
			hiddenVal[j] += (input[k]*mlp->weightsIH[k][j]);
		}
		hiddenVal[j] = sigmoid(hiddenVal[j]);
	}

	for(j=0; j<NUM_HIDDEN; j++){
		output += hiddenVal[j]*mlp->weightsHO[j];
	}

	//printf("output = %f\n", output);

	return output;
}

void MLP_print(MLP* mlp){
	int i, j;

	for(i=0; i<NUM_INPUTS; i++){
		for(j=0; j<NUM_HIDDEN; j++){
			printf("%.4f,", mlp->weightsIH[i][j]);
		}
		printf("\n");
	}
	printf("\n");

	for(i=0; i<NUM_HIDDEN; i++){
		printf("%.4f,", mlp->weightsHO[i]);
	}
	printf("\n");
}

double sigmoid(double x){
	return tanh(x);
}
